﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Drive_control_unit.InnerClass
{
    class Time
    {
        private Timer timer = new Timer();
        // Shared storage of data / Общее хранилище данных.
        private Storage data = null;

        public event EventHandler<EventTime> DataCalculated;
        
        // Number of operation / Номер операции.
        private short operation;
        // Parcel type / Тип посылки.
        private string parcel_type;

        // Starting and ending points / Начальная и конечная точки.
        private float startPoint, endPoint;
        // Number of shots / Количество снимков.
        private int count;
        // Operation time / Время выполнении операции.
        private float time;
        private DateTime dt;
        private char device;

        // Operation: 0 - Zero , 1 - Move ,2 - Survey , another - Error.
        private enum OperationType
        {
            zero,
            move,
            survey,
            error
        }

        public Time(ref Storage _data)
        {
            parcel_type = "data";
            data = _data;

            timer.Elapsed += Timer_Elapsed;

            operation = -1;
            time = 0;
            timer.AutoReset = true;
        }

        public void Stop()
        {
            timer.Stop();

            float distance = data.Speed * (float)(DateTime.Now - dt).TotalMilliseconds;

            switch (operation)
            {
                case (int)OperationType.zero:
                    {
                        if (distance > Math.Abs(data.TableWidth / 2 - data.EmitterX))
                        {
                            data.EmitterX = data.TableWidth / 2;
                        }
                        else
                        {
                            data.EmitterX = data.EmitterX > data.TableWidth / 2
                                ? data.EmitterX - distance
                                : data.EmitterX + distance;
                        }

                        if (distance > Math.Abs(data.TableWidth / 2 - data.DetectorX))
                        {
                            data.DetectorX = data.TableWidth / 2;
                        }
                        else
                        {
                            data.DetectorX = data.DetectorX > data.TableWidth / 2
                                ? data.DetectorX - distance
                                : data.EmitterX + distance;
                        }
                        break;
                    }
                case (int)OperationType.move:
                    {
                        if (device == 'd')
                        {
                            data.DetectorX = startPoint > endPoint
                                ? startPoint - distance
                                : startPoint + distance;

                        }
                        else
                        {
                            data.EmitterX = startPoint > endPoint
                                ? startPoint - distance
                                : startPoint + distance;
                        }
                        break;
                    }
                case (int)OperationType.survey:
                    {
                        data.DetectorX = startPoint > endPoint
                            ? startPoint - distance
                            : startPoint + distance;
                        data.EmitterX = startPoint > endPoint
                                ? startPoint - distance
                                : startPoint + distance;
                        break;
                    }
                default:
                    {
                        break;
                    }
            }

        }

        public float Zero()
        {
            operation = (int)OperationType.zero;
            // Calculation of the time of movement of the detector and emitter to the zero point.
            // Расчёт времени передвижения детектора и излучателя в нулевую точку.
            time = Math.Abs(data.TableWidth / 2 - data.EmitterX) > Math.Abs(data.TableWidth / 2 - data.DetectorX)
                ? Math.Abs(data.TableWidth / 2 - data.EmitterX) / data.Speed
                : Math.Abs(data.TableWidth / 2 - data.DetectorX) / data.Speed;

            dt = DateTime.Now;
            timer.Interval = time;
            timer.Start();

            return time;
        }

        public float Move(char _device, float _startPoint, float _endPoint)
        {
            if (!(_device == 'd' || _device == 'e')) return -1;

            operation = (int)OperationType.move;
            device = _device;
            startPoint = _startPoint;
            endPoint = _endPoint;

            time = Math.Abs(_startPoint - _endPoint) / data.Speed;

            dt = DateTime.Now;
            timer.Interval = time;
            timer.Start();

            return time;
        }

        public float Survey(float _startPoint, float _endPoint, int _count)
        {
            operation = (int)OperationType.survey;
            startPoint = _startPoint;
            endPoint = _endPoint;
            count = _count;

            data.DetectorX = startPoint;
            data.EmitterX = endPoint;

            time = Math.Abs(_startPoint - _endPoint) / data.Speed + 6000f / data.FramesPerSecond;

            dt = DateTime.Now;
            timer.Interval = time;
            timer.Start();

            return time;
        }


        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            switch (operation)
            {
                case (int)OperationType.zero:
                    {
                        data.DetectorX = data.TableWidth / 2;
                        data.EmitterX = data.TableWidth / 2;

                        operation = (int)OperationType.error;
                        timer.Stop();
                        break;
                    }
                case (int)OperationType.move:
                    {
                        if (device == 'd')
                        {
                            data.DetectorX = endPoint;

                            DataCalculated?.Invoke(this, new EventTime(parcel_type, OperationType.move.ToString(), device.ToString(), data.DetectorX.ToString(), data.Speed.ToString()));
                        }
                        else
                        {
                            data.EmitterX = endPoint;

                            DataCalculated?.Invoke(this, new EventTime(parcel_type, OperationType.move.ToString(), device.ToString(), data.EmitterX.ToString(), data.Speed.ToString()));
                        }



                        operation = (int)OperationType.error;
                        timer.Stop();
                        break;
                    }
                case (int)OperationType.survey:
                    {
                        string x = "";
                        float step = (endPoint - startPoint) / (count - 1);

                        for (int i = 0; i < count; i++)
                        {
                            x += (startPoint + (int)step * i).ToString();

                            if (i != count - 1)
                            {
                                x += ",";
                            }

                        }

                        data.DetectorX = endPoint;
                        data.EmitterX = startPoint;

                        DataCalculated?.Invoke(this, new EventTime(parcel_type, OperationType.survey.ToString(), device.ToString(), x.ToString()));
                        operation = (int)OperationType.error;
                        timer.Stop();
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
        }
    }

    public class EventTime : EventArgs
    {
        public string Command { get; }

        public EventTime(params string[] text)
        {
            var stringBuilder = new StringBuilder();

            for (var i = 0; i < text.Length; i++)
            {
                if (i != text.Length - 1)
                {
                    stringBuilder.Append($"{text[i]},");
                }
                else
                {
                    stringBuilder.Append($"{text[i]}]");
                }
            }

            Command = $"[{stringBuilder.ToString()}";
        }
    }
}