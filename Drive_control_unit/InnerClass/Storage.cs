﻿namespace Drive_control_unit.InnerClass
{
    class Storage
    {
        private float _condition,
                    _speed,
                    _tableWidth,
                    _detectorX,
                    _emitterX;
        private short _framesPerSecond;

        public Storage()
        {

        }

        public float Condition { get; set; }

        public float Speed { get; set; }

        public float TableWidth { get; set; }

        public float DetectorX { get; set; }

        public float EmitterX { get; set; }

        public short FramesPerSecond { get; set; }
    }
}