﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Drive_control_unit.InnerClass
{
    class CommunicationProtocol
    {
        private bool flagIncompleteCommand;
        private string IncompleteCommand,
                       parcel_type;

        private float timer;
        private const int CommandNotFound = -1;

        private Storage data = null;
        private Time time;

        public event EventHandler<EventTime> DataCalculated;

        private enum ActionType
        {
            status,
            stop,
            zero,
            move,
            survey
        }

        public CommunicationProtocol(ref Storage _data)
        {
            // Default settings / Настройки по умолчанию.
            flagIncompleteCommand = false;
            IncompleteCommand = "";
            // Constant / Константа.
            parcel_type = "reply";
            // Initialize storage / Инициализация хранилища.
            data = _data;
            time = new Time(ref data);
            time.DataCalculated += Time_DataCalculated;
            timer = 0;
        }

        private void Time_DataCalculated(object sender, EventTime eventTime)
        {
            DataCalculated?.Invoke(sender, eventTime);
        }

        public string[] ReceivingСommands(string message)
        {
            if (flagIncompleteCommand) message = IncompleteCommand + message;
            flagIncompleteCommand = false;
            IncompleteCommand = "";

            string[] commands = null;

            // Если сообщение не имеет команд скобок, то запоминает строку и выходим из функции. 
            if ((message.IndexOf('[') == CommandNotFound) && (message.IndexOf(']') == CommandNotFound))
            {
                flagIncompleteCommand = true;
                IncompleteCommand = message;
                return commands;
            }

            // If the last command is not completed, then the rest of the command is written in a separate line.
            // Если последняя команда не полная, то записываем в отдельную строку остаток команды.
            if (message[message.Length - 1] != ']')
            {
                flagIncompleteCommand = true;
                IncompleteCommand = message.Substring(message.LastIndexOf('['));
                message = message.Substring(0, message.LastIndexOf('['));
            }
            // We remove Cut the initial useless characters.
            // Вырезаем начальные бесполезные символы.
            commands = message.Substring(message.IndexOf('[') + 1, message.Length - (message.IndexOf('[') + 1)).Split('[');

            // We split the command string into an array of string commands.
            // Разбиваем строку команд на массив строковых команд.
            for (int i = 0; i < commands.Length; i++)
            {
                commands[i] = commands[i].Substring(0, commands[i].IndexOf(']'));
            }


            return CheackCommands(commands);
        }

        private string[] CheackCommands(string[] commands)
        {
            List<string> list = new List<string>();
            bool error;
            // If the command has a character that does not belong to numbers, Latin, comma or period, 
            // then the command is not taken into account.
            // Если в команде есть символ, не принадлежащий цифрам, латинице, запятой или точке, 
            // то команда не учитывается.
            foreach (string com in commands)
            {
                error = false;
                for (int i = 0; i < com.Length; i++)
                {
                    if ((com[i] < 'a' || com[i] > 'z') && (com[i] < 'A' || com[i] > 'Z') && (com[i] < '0' || com[i] > '9') && (com[i] != '.') && (com[i] != ',') && (com[i] != ' '))
                    {
                        error = true;
                        break;
                    }
                }

                if (!error)
                {
                    list.Add(com);
                }
            }

            commands = list.ToArray();

            return commands;
        }


        public float CommandHandler(string command)
        {
            // We get the processed arguments of the command without request.
            // Получаем обработанные аргументы команды без request.
            string[] arguments = SeparationCommand(command);
            timer = 0;

            switch (arguments[0])
            {
                case "status":
                    {
                        timer = 0;
                        DataCalculated?.Invoke(this, new EventTime(parcel_type, ActionType.status.ToString(), data.Condition.ToString()));//($"[reply,{((ActionType)ActionType.status).ToString()},{data.Condition}]"));
                        break;
                    }
                case "stop":
                    {
                        time.Stop();
                        timer = 0;
                        DataCalculated?.Invoke(this, new EventTime(parcel_type, ActionType.stop.ToString(), data.Condition.ToString()));// $"[reply,stop,{data.Condition}]"));
                        break;
                    }
                case "zero":
                    {
                        timer = time.Zero();
                        DataCalculated?.Invoke(this, new EventTime(parcel_type, ActionType.zero.ToString(), data.Condition.ToString()));//$"[reply,zero,{data.Condition}]"));
                        break;
                    }
                case "move":
                    {
                        timer = time.Move(arguments[1][0], Convert.ToSingle(arguments[2]), Convert.ToSingle(arguments[3]));
                        DataCalculated?.Invoke(this, new EventTime(parcel_type, ActionType.move.ToString(), data.Condition.ToString()));//$"[reply,move,{data.Condition}]"));
                        break;
                    }
                case "survey":
                    {
                        timer = time.Survey(Convert.ToSingle(arguments[1]), Convert.ToSingle(arguments[2]), Convert.ToInt32(arguments[3]));
                        DataCalculated?.Invoke(this, new EventTime(parcel_type, ActionType.survey.ToString(), data.Condition.ToString())); //"reply", "survey", data.Condition.ToString()));// ($"[reply,survey,{data.Condition}]"));
                        break;
                    }
                default:

                    break;
            }

            return timer;
        }


        private string[] SeparationCommand(string command)
        {
            // If a command has less than 2 arguments and more than 5 arguments and the first argument is not a request, 
            // then the command is not taken into account.
            // Если в команде меньше 2 аргументов и больше 5 аргументов и первый аргумент не request, 
            // то команда не учитывается.
            string[] arguments = command.Split(',');
            string[] list = new string[arguments.Length - 1];
            // Remove spaceы / Убираем пробелы.
            for (int i = 0; i < arguments.Length; i++)
            {
                arguments[i] = arguments[i].Trim();
            }

            if ((arguments.Length >= 2 && arguments.Length <= 5) && (arguments[0] == "request"))
            {
                // Remove argument request / Убираем аргумент request.
                for (int i = 1; i < arguments.Length; i++)
                {
                    list[i - 1] = arguments[i];
                }
            }

            return list;
        }
    }
}