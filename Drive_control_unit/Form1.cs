﻿using Drive_control_unit.InnerClass;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Drive_control_unit
{
    public partial class Form1 : Form
    {
        public string[] ActivePorts;

        private int Button_Distance,
                    ComboBox_Distance,
                    LeftTextBoxs_Distance,
                    RightTextBoxs_Distance;
        int counter;

        private enum MessageType
        {
            port_operations = 'O',
            incoming = '>',
            outgoint = '<',
            user_action = '!'
        }

        static Storage data;
        SerialPort port = new SerialPort();
        CommunicationProtocol protocol;

        public Form1()
        {
            // Initialize counter for grid / Инициализация счетчика для таблицы.
            counter = 0;
            InitializeComponent();
            // Initialize Com-Port / Инициализация Ком-Порта.
            InitializeComPort();
            // We get list of avtive ports / Получаем список активных портов.
            ActivePorts = SerialPort.GetPortNames();
            // Refresh comboBox_ComPorts / Обновляем ComboBox.
            RefreshCombobox();

            // Creating and setting up a table / Создание и настройка таблицы.
            SettingDataGrid();

            // Customize a form / Настройка формы.
            SettingForm();

            protocol.DataCalculated += (s, e) =>
            {
                AddToGrid((char)(MessageType.outgoint), e.Command);
            };
        }

        private void SettingForm()
        {
            textBox1.Text = Properties.Settings.Default.DetectorX;
            textBox2.Text = Properties.Settings.Default.EmitterX;
            textBox3.Text = Properties.Settings.Default.TableWidth;
            textBox4.Text = Properties.Settings.Default.Condition;
            textBox5.Text = Properties.Settings.Default.Speed;
            textBox6.Text = Properties.Settings.Default.FramesPerSecond;

            // Create a storage / Создание хранилища.
            data = new Storage();
            protocol = new CommunicationProtocol(ref data);

            // Сохраненные настройки.
            try
            {
                data.DetectorX = Convert.ToSingle(Properties.Settings.Default.DetectorX);
                data.EmitterX = Convert.ToSingle(Properties.Settings.Default.EmitterX);
                data.TableWidth = Convert.ToSingle(Properties.Settings.Default.TableWidth);
                data.Condition = Convert.ToUInt16(Properties.Settings.Default.Condition);
                data.Speed = Convert.ToSingle(Properties.Settings.Default.Speed.Replace('.', ','));
                data.FramesPerSecond = Convert.ToInt16(Properties.Settings.Default.FramesPerSecond);
            }
            catch
            {
                // Default settings / Настройки по умолчанию.
                data.Condition = 1;
                data.TableWidth = 2000;
                data.DetectorX = 1000;
                data.EmitterX = 1000;
                data.Speed = 0.1f;
                data.FramesPerSecond = 61;
            }
            // Настройка минимального размера формы.
            MinimumSize = Size;
            // Расстояние элементов от правого края.
            Button_Distance = ClientSize.Width - button_OpenOrClose.Left - button_OpenOrClose.Width;
            ComboBox_Distance = ClientSize.Width - comboBox_ComPorts.Left - comboBox_ComPorts.Width;

            LeftTextBoxs_Distance = ClientSize.Width - (int)(3f / 7 * (panel2.Width));
            RightTextBoxs_Distance = ClientSize.Width - (int)(6f / 7 * (panel2.Width));

            ClientSizeChanged += Form1_ClientSizeChanged;
            // Эмитируем событие.
            ClientSize = ClientSize;
        }

        private void Form1_ClientSizeChanged(object sender, EventArgs e)
        {
            // Настройка местоположение, длины и ширины элементов формы.
            // Настройка по X.
            panel1.Width = this.ClientSize.Width - 2 * panel1.Left;
            panel2.Width = ClientSize.Width - 2 * panel2.Left;

            button_OpenOrClose.Left = ClientSize.Width - button_OpenOrClose.Width - Button_Distance;
            comboBox_ComPorts.Left = ClientSize.Width - comboBox_ComPorts.Width - ComboBox_Distance;

            textBox1.Left = ClientSize.Width - (int)(3f / 4 * panel2.Width);
            textBox2.Left = ClientSize.Width - (int)(3f / 4 * panel2.Width);
            textBox3.Left = ClientSize.Width - (int)(3f / 4 * panel2.Width);

            textBox4.Left = ClientSize.Width - (int)(1f / 4 * panel2.Width);
            textBox5.Left = ClientSize.Width - (int)(1f / 4 * panel2.Width);
            textBox6.Left = ClientSize.Width - (int)(1f / 4 * panel2.Width);

            label1.Left = textBox1.Left - 5 - label1.Width;
            label2.Left = textBox2.Left - 5 - label2.Width;
            label3.Left = textBox3.Left - 5 - label3.Width;
            label4.Left = textBox4.Left - 5 - label4.Width;
            label5.Left = textBox5.Left - 5 - label5.Width;
            label6.Left = textBox6.Left - 5 - label6.Width;

            dataGridView1.Width = ClientSize.Width - 2 * dataGridView1.Left;
            dataGridView1.Columns[2].Width = dataGridView1.Width - dataGridView1.Columns[0].Width - dataGridView1.Columns[1].Width - 3;

            // Настройка по Y.
            panel2.Height = ClientSize.Height / 2;
            dataGridView1.Top = panel2.Top + panel2.Height + 5;
            dataGridView1.Height = ClientSize.Height - (panel2.Top + panel2.Height + 10);

            textBox1.Top = ((panel2.Height / 4) - textBox1.Height / 2);
            textBox2.Top = ((panel2.Height * 2 / 4) - textBox2.Height / 2);
            textBox3.Top = ((panel2.Height * 3 / 4) - textBox3.Height / 2);

            textBox4.Top = ((panel2.Height / 4) - textBox4.Height / 2);
            textBox5.Top = ((panel2.Height * 2 / 4) - textBox5.Height / 2);
            textBox6.Top = ((panel2.Height * 3 / 4) - textBox6.Height / 2);

            label1.Top = textBox1.Top;
            label2.Top = textBox2.Top;
            label3.Top = textBox3.Top;

            label4.Top = textBox4.Top;
            label5.Top = textBox5.Top;
            label6.Top = textBox6.Top;
        }


        private void InitializeComPort()
        {
            port.BaudRate = 115200;
            port.DataBits = 8;
            port.Parity = Parity.None;
            port.StopBits = StopBits.One;
            port.ReadTimeout = 1000;
            port.WriteTimeout = 1000;
            // Create an event, which works when dates is coming on Com-Port / Создания события при поступлении данных на Com-Port.
            port.DataReceived += Data_Acquisition;
        }
        private void SettingDataGrid()
        {
            dataGridView1.ColumnCount = 3;

            dataGridView1.ColumnHeadersDefaultCellStyle.Font = new Font(dataGridView1.Font, FontStyle.Bold);
            dataGridView1.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
            dataGridView1.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            dataGridView1.Columns[0].Name = "Type";
            dataGridView1.Columns[0].Width = 40;
            dataGridView1.Columns[1].Name = "Time";
            dataGridView1.Columns[2].Name = "Message";
            dataGridView1.Columns[2].Width = dataGridView1.Width - dataGridView1.Columns[0].Width - dataGridView1.Columns[1].Width - 3;

            dataGridView1.RowHeadersVisible = false;
        }
        // Update combobox dropdown list / Обновление выпадающего списка в combobox.
        public void RefreshCombobox()
        {
            comboBox_ComPorts.Items.Clear();
            foreach (string Port in ActivePorts)
            {
                comboBox_ComPorts.Items.Add(Port);
                if (Properties.Settings.Default.SelectedPort == Port) comboBox_ComPorts.SelectedItem = Port;
            }
        }
        private void Data_Acquisition(object sender, SerialDataReceivedEventArgs e)
        {
            if (!port.IsOpen) return;

            // Read data / Считывание данных.
            string message = port.ReadExisting();

            string[] AllCommands = protocol.ReceivingСommands(message);

            // Если команд нет, то прекращается работа функции.
            if (AllCommands == null)
            {
                return;
            }

            foreach (string oneCommand in AllCommands)
            {
                AddToGrid((char)MessageType.incoming, $"[{oneCommand}]");
                protocol.CommandHandler(oneCommand);
            }
        }


        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Enter) return;
            try
            {
                data.DetectorX = Convert.ToSingle(textBox1.Text);
                Properties.Settings.Default.DetectorX = textBox1.Text;
                Properties.Settings.Default.Save();
                AddToGrid((char)MessageType.user_action, $"DetectorX {textBox1.Text}");
            }
            catch { }
        }

        private void textBox2_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Enter) return;
            try
            {
                data.EmitterX = Convert.ToSingle(textBox2.Text);
                Properties.Settings.Default.EmitterX = textBox2.Text;
                Properties.Settings.Default.Save();
                AddToGrid((char)MessageType.user_action, $"EmitterX {textBox2.Text}");
            }
            catch { }
        }

        private void textBox3_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Enter) return;
            try
            {
                data.TableWidth = Convert.ToSingle(textBox3.Text);
                Properties.Settings.Default.TableWidth = textBox3.Text;
                Properties.Settings.Default.Save();
                AddToGrid((char)MessageType.user_action, $"TableWidth {textBox3.Text}");
            }
            catch { }
        }

        private void textBox4_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Enter) return;
            try
            {
                data.Condition = Convert.ToSingle(textBox4.Text);
                Properties.Settings.Default.Condition = textBox4.Text;
                Properties.Settings.Default.Save();
                AddToGrid((char)MessageType.user_action, $"Condition {textBox4.Text}");
            }
            catch { }
        }

        private void textBox5_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Enter) return;
            try
            {
                data.Speed = Convert.ToSingle(textBox5.Text.Replace('.', ','));
                Properties.Settings.Default.Speed = textBox5.Text;
                Properties.Settings.Default.Save();
                AddToGrid((char)MessageType.user_action, $"Speed {textBox5.Text}");
            }
            catch { }
        }

        private void textBox6_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Enter) return;
            try
            {
                data.FramesPerSecond = Convert.ToInt16(textBox6.Text);
                Properties.Settings.Default.FramesPerSecond = textBox6.Text;
                Properties.Settings.Default.Save();
                AddToGrid((char)MessageType.user_action, $"FramesPerSecond {textBox6.Text}");
            }
            catch { }
        }

        private void AddToGrid(char Type, string Command)
        {
            Dispatcher.Invoke(this, () =>
             {
                 dataGridView1.Rows.Add();
                 dataGridView1.Rows[counter].Cells[0].Value = Type.ToString();
                 dataGridView1.Rows[counter].Cells[1].Value = DateTime.Now.ToLongTimeString();
                 dataGridView1.Rows[counter++].Cells[2].Value = Command;

                 // Прокрутка scroll в конец.
                 dataGridView1.FirstDisplayedScrollingRowIndex = dataGridView1.RowCount - 1;
             });
        }
        private void button_OpenOrClose_Click(object sender, EventArgs e)
        {
            if (!port.IsOpen)
            {
                // When Com-Port opened.
                // WinForm.
                comboBox_ComPorts.Enabled = false;
                label_PortStatus.Text = "Порт: Открыт";
                button_OpenOrClose.Text = "Закрыть";
                // Backend.
                port.Open();

                AddToGrid((char)MessageType.port_operations, comboBox_ComPorts.SelectedItem.ToString() + " Открыт");
            }
            else
            {
                // When Com-Port closed.
                // WinForm.
                port.Close();
                comboBox_ComPorts.Enabled = true;
                label_PortStatus.Text = "Порт: Закрыт";
                button_OpenOrClose.Text = "Открыть";
                // Backend.
                port.Close();

                AddToGrid((char)MessageType.port_operations, comboBox_ComPorts.SelectedItem.ToString() + " Закрыт");
            }
        }

        private void comboBox_ComPorts_SelectedIndexChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.SelectedIndex = comboBox_ComPorts.SelectedIndex;
            try
            {
                Properties.Settings.Default.SelectedPort = comboBox_ComPorts.SelectedItem.ToString();
                port.PortName = comboBox_ComPorts.Text;
            }
            catch { }

            Properties.Settings.Default.Save();
        }
    }

    public delegate void DispatcherInvoker(Form form, Action a);

    public class Dispatcher
    {
        public static void Invoke(Form form, Action action)
        {
            if (!form.InvokeRequired)
            {
                action();
            }
            else
            {
                form.Invoke((DispatcherInvoker)Invoke, form, action);
            }
        }
    }
}